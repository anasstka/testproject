package dev.anasstka.exam.network.model

import com.google.gson.annotations.SerializedName

data class RoomResponse(
    @SerializedName("items")
    val items: List<Room>,
)

data class Room(
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
)