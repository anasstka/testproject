package dev.anasstka.exam.network

import dev.anasstka.exam.network.model.RoomResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface ApiService {

    @GET("/rooms")
    fun getRooms(
        @Header("token")
        token: String,
        @Header("uuid")
        uuid: String,
        @Header("hash")
        hash: String? = null,
    ): Call<RoomResponse>
}