package dev.anasstka.exam.util

import dev.anasstka.exam.R

enum class TypeRoom(val type: String, val iconId: Int) {
    KITCHEN("Кухня", R.drawable.ic_room_kitchen),
    BEDROOM("Спальня", R.drawable.ic_room_bedroom),
    BATHROOM("Ванна", R.drawable.ic_room_bathroom),
    OFFICE("Кабинет", R.drawable.ic_room_office),
    TV_ROOM("Кинотеатр", R.drawable.ic_tv_room),
    LIVING_ROOM("Комната отдыха", R.drawable.ic_living_room),
    GARAGE("Гараж", R.drawable.ic_room_garage),
    TOILET("Туалет", R.drawable.ic_room_toilet),
    KID_ROOM("Детская", R.drawable.ic_kids_room),
}

fun getTypeRoomByName(nameType: String): TypeRoom {
    return when (nameType.lowercase()) {
        TypeRoom.KITCHEN.type.lowercase() -> TypeRoom.KITCHEN
        TypeRoom.BEDROOM.type.lowercase() -> TypeRoom.BEDROOM
        TypeRoom.BATHROOM.type.lowercase() -> TypeRoom.BATHROOM
        TypeRoom.OFFICE.type.lowercase() -> TypeRoom.OFFICE
        TypeRoom.TV_ROOM.type.lowercase() -> TypeRoom.TV_ROOM
        TypeRoom.LIVING_ROOM.type.lowercase() -> TypeRoom.LIVING_ROOM
        TypeRoom.GARAGE.type.lowercase() -> TypeRoom.GARAGE
        TypeRoom.TOILET.type.lowercase() -> TypeRoom.TOILET
        TypeRoom.KID_ROOM.type.lowercase() -> TypeRoom.KID_ROOM
        else -> TypeRoom.KITCHEN
    }
}