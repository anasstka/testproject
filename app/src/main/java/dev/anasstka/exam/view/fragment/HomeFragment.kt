package dev.anasstka.exam.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dev.anasstka.exam.R
import dev.anasstka.exam.network.ApiHandler
import dev.anasstka.exam.network.ApiService
import dev.anasstka.exam.network.model.Room
import dev.anasstka.exam.view.activity.AddRoomActivity
import dev.anasstka.exam.view.adapter.RoomAdapter

class HomeFragment : Fragment() {

    var service: ApiService = ApiHandler.instance.service

    lateinit var rvRooms: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(
            R.layout.fragment_home,
            container,
            false
        )

        rvRooms = view.findViewById(R.id.rv_rooms)
        rvRooms.layoutManager = GridLayoutManager(
            context,
            2,
            GridLayoutManager.VERTICAL,
            false
        )
        getRooms()

        (view.findViewById<ImageButton>(R.id.btn_addRoom)).setOnClickListener {
            startActivity(Intent(requireActivity(), AddRoomActivity::class.java))
        }

        return view
    }

    private fun getRooms() {
        val rooms = listOf(
            Room(name = "Гостевая", type = "Комната отдыха"),
            Room(name = "Кушац", type = "Кухня"),
            Room(name = "Мыться", type = "Ванна"),
            Room(name = "Спать", type = "Спальня"),
        )

        rvRooms.adapter = RoomAdapter(rooms)

//        AsyncTask.execute {
//            service.getRooms("", "").enqueue(object : Callback<RoomResponse> {
//                override fun onResponse(
//                    call: Call<RoomResponse>,
//                    response: Response<RoomResponse>
//                ) {
//                    if (response.isSuccessful) {
//                        val roomResponse = response.body()
//                        if (roomResponse != null)
//                            rvRooms.adapter = RoomAdapter(roomResponse.items)
//                    } else {
//        dialog(requireActivity(), "Ошибка сервера")
//                    }
//                }
//
//                override fun onFailure(call: Call<RoomResponse>, t: Throwable) {
//
//                }
//            })
//        }
    }
}