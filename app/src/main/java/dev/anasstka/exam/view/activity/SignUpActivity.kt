package dev.anasstka.exam.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import dev.anasstka.exam.R
import dev.anasstka.exam.view.custom.dialog
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.btn_enterYourHome
import kotlinx.android.synthetic.main.activity_sign_up.btn_newResident

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        btn_enterYourHome.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
        }

        btn_newResident.setOnClickListener {
            doSignUp()
        }
    }

    private fun doSignUp() {
        if (!isValidField())
            return

        startActivity(Intent(this, MainActivity::class.java))
//        AsyncTask
    }

    private fun isValidField(): Boolean {
        if (et_signUpEmail.text.toString().isBlank()) {
            dialog(this@SignUpActivity, "Поле E-mail не может быть пустым")
            return false
        }
        if (et_signUpPassword.text.toString().isBlank()) {
            dialog(this@SignUpActivity, "Поле Password не может быть пустым")
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(et_signUpEmail.text.toString()).matches()) {
            dialog(this@SignUpActivity, "Некорректный E-mail")
            return false
        }

        return true
    }
}