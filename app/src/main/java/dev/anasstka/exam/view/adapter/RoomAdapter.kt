package dev.anasstka.exam.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dev.anasstka.exam.R
import dev.anasstka.exam.network.model.Room
import dev.anasstka.exam.util.getTypeRoomByName

class RoomAdapter(
    private val rooms: List<Room>
) : RecyclerView.Adapter<RoomAdapter.RoomViewHolder>() {

    class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ivRoomIcon: ImageView
        var tvRoomName: TextView
        var tvRoomDeviceCount: TextView

        init {
            ivRoomIcon = view.findViewById(R.id.iv_roomIcon)
            tvRoomName = view.findViewById(R.id.tv_roomName)
            tvRoomDeviceCount = view.findViewById(R.id.tv_roomDeviceCount)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val itemView: View = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_room,
                parent,
                false
            )
        return RoomViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        val room = rooms[position]
        val typeRoom = getTypeRoomByName(room.type)

        holder.ivRoomIcon.setImageResource(typeRoom.iconId)
        holder.tvRoomName.text = room.name

        holder.itemView.setOnClickListener { view ->
//            val intent = Intent(view.context, ChatActivity::class.java)
//            intent.putExtra("movieId", movie.movieId)
//            intent.putExtra("movieName", movie.name)
//            view.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return rooms.size
    }
}