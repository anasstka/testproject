package dev.anasstka.exam.view.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dev.anasstka.exam.R
import dev.anasstka.exam.util.TypeRoom

class SelectRoomAdapter(
    private val rooms: List<TypeRoom>
) : RecyclerView.Adapter<SelectRoomAdapter.RoomViewHolder>() {

    var selectedItem: Int = -1

    class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var container: LinearLayout
        var ivRoomIcon: ImageView
        var tvRoomType: TextView

        init {
            container = view.findViewById(R.id.container_back)
            ivRoomIcon = view.findViewById(R.id.iv_roomIcon)
            tvRoomType = view.findViewById(R.id.tv_roomType)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val itemView: View = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_select_room,
                parent,
                false
            )
        return RoomViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        val room = rooms[position]

        holder.ivRoomIcon.setImageResource(room.iconId)
        holder.tvRoomType.text = room.type

        if (position == selectedItem) {
            holder.container.backgroundTintList = ColorStateList.valueOf(0x984E4F)
            holder.ivRoomIcon.imageTintList = ColorStateList.valueOf(0xFFFFFF)
            holder.tvRoomType.setTextColor(0x984E4F)
        } else {
            holder.container.backgroundTintList = ColorStateList.valueOf(0xF0F0F0)
            holder.ivRoomIcon.imageTintList = ColorStateList.valueOf(0xAFAFAF)
            holder.tvRoomType.setTextColor(0x2A2A37)
        }

        holder.itemView.setOnClickListener { view ->
            selectedItem = position
        }
    }

    override fun getItemCount(): Int {
        return rooms.size
    }
}