package dev.anasstka.exam.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dev.anasstka.exam.R
import dev.anasstka.exam.util.TypeRoom
import dev.anasstka.exam.view.adapter.SelectRoomAdapter

class AddRoomActivity : AppCompatActivity() {

    lateinit var rvSelectRooms: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_room)

        rvSelectRooms = findViewById(R.id.rv_selectRooms)
        rvSelectRooms.layoutManager = GridLayoutManager(
            applicationContext,
            3,
            GridLayoutManager.VERTICAL,
            false
        )
        getRooms()
    }

    private fun getRooms() {
        val rooms = listOf(
            TypeRoom.KITCHEN,
            TypeRoom.BEDROOM,
            TypeRoom.BATHROOM,
            TypeRoom.OFFICE,
            TypeRoom.TV_ROOM,
            TypeRoom.LIVING_ROOM,
            TypeRoom.GARAGE,
            TypeRoom.TOILET,
            TypeRoom.KID_ROOM,
        )

        rvSelectRooms.adapter = SelectRoomAdapter(rooms)
    }
}