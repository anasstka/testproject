package dev.anasstka.exam.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import dev.anasstka.exam.R
import dev.anasstka.exam.view.fragment.HomeFragment
import dev.anasstka.exam.view.fragment.RoutinesFragment
import dev.anasstka.exam.view.fragment.SettingFragment
import dev.anasstka.exam.view.fragment.StaticsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val staticsFragment = StaticsFragment()
        val routinesFragment = RoutinesFragment()
        val settingFragment = SettingFragment()

        setCurrentFragment(homeFragment)

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> setCurrentFragment(homeFragment)
                R.id.statics -> setCurrentFragment(staticsFragment)
                R.id.routines -> setCurrentFragment(routinesFragment)
                R.id.settings -> setCurrentFragment(settingFragment)

            }
            true
        }

    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment, fragment)
            commit()
        }

}