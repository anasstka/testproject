package dev.anasstka.exam.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import dev.anasstka.exam.R
import dev.anasstka.exam.view.custom.dialog
import kotlinx.android.synthetic.main.activity_sign_in.*


class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        btn_enterYourHome.setOnClickListener {
            doSignIn()
        }

        btn_newResident.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
    }

    private fun doSignIn() {
        if (!isValidField())
            return

//        AsyncTask
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun isValidField(): Boolean {
        if (et_signInEmail.text.toString().isBlank()) {
            dialog(this@SignInActivity, "Поле E-mail не может быть пустым")
            return false
        }
        if (et_signInName.text.toString().isBlank()) {
            dialog(this@SignInActivity, "Поле Name не может быть пустым")
            return false
        }
        if (et_signInPassword.text.toString().isBlank()) {
            dialog(this@SignInActivity, "Поле Password не может быть пустым")
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(et_signInEmail.text.toString()).matches()) {
            dialog(this@SignInActivity, "Некорректный E-mail")
            return false
        }

        return true
    }
}